/*
 * DicomEdit: TestFunctions
 * XNAT http://www.xnat.org
 * Copyright (c) 2017, Washington University School of Medicine
 * All Rights Reserved
 *
 * Released under the Simplified BSD.
 */

package org.nrg.dicom.dicomedit;

import org.junit.Test;
import org.nrg.dicom.mizer.exceptions.MizerException;
import org.nrg.dicom.mizer.objects.DicomObjectFactory;
import org.nrg.dicom.mizer.objects.DicomObjectI;
import org.nrg.dicom.mizer.tags.TagPath;

import java.io.ByteArrayInputStream;

import static org.junit.Assert.assertEquals;
import static org.junit.Assert.assertTrue;

/**
 * Run tests of TagPathFactory.
 *
 */
public class TestTagPathFactory {

    @Test
    public void testSimpleTag() throws MizerException {

        final TagPath tp = TagPathFactory.createInstance( "(0020,0020)");

        assertEquals( "00200020", tp.getPath());
    }

    @Test
    public void testPrivateTag() throws MizerException {

        final TagPath tp = TagPathFactory.createInstance( "(0029,{SIEMENS CSA}XX)");

        assertEquals( "0029\"SIEMENS CSA\"10XX", tp.getPath());
    }

    @Test
    public void testSimpleSequeneTag() throws MizerException {

        final TagPath tp = TagPathFactory.createInstance( "(0032,1024)/(0008,1032)");

        assertEquals( "00321024[%]/00081032", tp.getPath());
    }

}
