/*
 * DicomEdit: ScriptRunner
 * XNAT http://www.xnat.org
 * Copyright (c) 2017, Washington University School of Medicine
 * All Rights Reserved
 *
 * Released under the Simplified BSD.
 */

package org.nrg.dicom.dicomedit;

import org.nrg.dicom.mizer.exceptions.MizerException;

import java.io.*;
import java.nio.file.Path;
import java.nio.file.Paths;

/**
 * An app to quickly apply specified scripts to specified data.
 *
 * First argument is path to the anon script.
 * Second argument is path to file or directory to recurse and apply transformation.
 * Third argument is root path to output directory.
 *
 */
public class ScriptRunner {

    public static void main(final String args[]) {
        final ScriptApplicator applicator;

        try {
            try (InputStream is = (0 == args.length || "-".equals(args[0]) ? System.in : new FileInputStream(args[0]))) {
//            try (InputStream is = new ByteArrayInputStream("(0080,1110)/(0010,0010)/(0019,[SIEMENS]10) := (0010,0020)\n".getBytes())) {
//            try (InputStream is = new ByteArrayInputStream("(0080,1110)[0]/(0010,0010)[0]/(0019,[SIEMENS]10) := (0010,0020)\n".getBytes())) {
//            try (InputStream is = new ByteArrayInputStream("(0080,1110)[0]/(0010,0010)[0]/(0019,[SIEMENS]10) := (0054,0410)[0]/(0054,0412)[0]/(0008,0105)\n".getBytes())) {
//            try (InputStream is = new ByteArrayInputStream("(0008,0020) ~ \"20\\\\d\\\\d\\\\d\\\\d\\\\d\\\\d\" : (0010,4000) := \"Session 20XX\"\n".getBytes())) {
                applicator = new ScriptApplicator(is);
            }

            File inputFile = new File( args[1]);
            Path outRootPath = Paths.get( args[2]);
            Path inRootPath  = null;
            if( inputFile.isDirectory()) {
                inRootPath = Paths.get(inputFile.getAbsolutePath());
            }
            else {
                inRootPath = Paths.get( inputFile.getParentFile().getAbsolutePath());
            }
            FileMapperI fileMapper = new RootMapper( inRootPath, outRootPath);

            processFile(inputFile, applicator, fileMapper);

        } catch (Exception e) {
            e.printStackTrace();
        }
    }

    private static void processFile( File f, ScriptApplicator applicator, FileMapperI fileMapper) throws MizerException, IOException {
        if( f.isDirectory()) {
            for( File ff: f.listFiles()) {
                processFile( ff, applicator, fileMapper);
            }
        }
        else {
            if( f.getName().endsWith(".dcm")) {
                File outFile = fileMapper.map(f);
                if (!outFile.getParentFile().exists()) {
                    outFile.getParentFile().mkdirs();
                }
                try (FileOutputStream out = new FileOutputStream( outFile)) {
                    applicator.apply(f).write(out);
                }
            }
        }
    }

    private interface FileMapperI {
        Path map( Path inputPath);
        File map( File file);
    }

    private static class RootMapper implements FileMapperI {
        private final Path inRootPath;
        private final Path outRootPath;

        public RootMapper( Path inRootPath, Path outRootPath) {
            this.inRootPath = inRootPath;
            this.outRootPath = outRootPath;
        }

        public Path map( Path inputPath) {
            return outRootPath.resolve( inRootPath.relativize( inputPath));
        }
        public File map( File inFile) {
            return map( Paths.get( inFile.getAbsolutePath())).toFile();
        }
    }

    private static File mapFileName(File f) {
        final String presuffix = "-mod";
        final File dir = f.getParentFile();
        final StringBuilder name = new StringBuilder(f.getName());
        final int presuffixLoc = name.lastIndexOf(".");
        if (presuffixLoc < 0) {
            name.append(presuffix);
        } else {
            name.insert(presuffixLoc, presuffix);
        }
        return new File(dir, name.toString());
    }

}
