/*
 * DicomEdit: TestFunctions
 * XNAT http://www.xnat.org
 * Copyright (c) 2017, Washington University School of Medicine
 * All Rights Reserved
 *
 * Released under the Simplified BSD.
 */

package org.nrg.dicom.dicomedit;

import org.junit.Test;
import org.nrg.dicom.mizer.exceptions.MizerException;
import org.nrg.dicom.mizer.objects.DicomObjectFactory;
import org.nrg.dicom.mizer.objects.DicomObjectI;
import org.nrg.test.workers.resources.ResourceManager;

import java.io.ByteArrayInputStream;
import java.io.File;

import static org.junit.Assert.assertEquals;
import static org.junit.Assert.assertFalse;
import static org.junit.Assert.assertTrue;

/**
 * Run tests of hashReferencedUIDs function.
 *
 */
public class TestHashReferencedUIDsFunction {

//    private static final ResourceManager _resourceManager = ResourceManager.getInstance();
//    private static final File FILE4 = _resourceManager.getTestResourceFile("dicom/1.2.840.113717.2.21733635.1-3-2-7b04bw.dcm");
//    private static final File FILE4 = _resourceManager.getTestResourceFile("short.dcm");

    /**
     * Test 1 level sequence with multiple items.
     *
     * @throws MizerException
     */
    @Test
    public void testStructureSetROISequence() throws MizerException {

        String script = "uidprefix := \"1.23.4\" \n" +
                "hashReferencedUIDs[ uidprefix, \"(3006, 0020)/(3006, 0024)\"]";

        final DicomObjectI src_dobj = createSeqTestObject();

        assertEquals( "PatientName", src_dobj.getString(s));

        assertEquals( "", src_dobj.getString(s0_1));
        assertEquals( "1", src_dobj.getString(s0_2));
        assertEquals( "1.3.12.2.1107.5.2.6.22750.20111117084936000.0.0.0", src_dobj.getString(s0_3));
        assertEquals( "Tum1_LFront", src_dobj.getString(s0_4));
        assertEquals( "MANUAL", src_dobj.getString(s0_5));

        assertEquals( "", src_dobj.getString(s1_1));
        assertEquals( "2", src_dobj.getString(s1_2));
        assertEquals( "1.3.12.2.1107.5.2.6.22750.20111117084936000.0.0.0", src_dobj.getString(s1_3));
        assertEquals( "Tum2_Vermis", src_dobj.getString(s1_4));
        assertEquals( "MANUAL", src_dobj.getString(s1_5));

        assertEquals( "", src_dobj.getString(s2_1));
        assertEquals( "3", src_dobj.getString(s2_2));
        assertEquals( "1.3.12.2.1107.5.2.6.22750.20111117084936000.0.0.0", src_dobj.getString(s2_3));
        assertEquals( "Tum3_RFront", src_dobj.getString(s2_4));
        assertEquals( "MANUAL", src_dobj.getString(s2_5));

        final ScriptApplicator sa = new ScriptApplicator(bytes(script));
        final DicomObjectI result_dobj = sa.apply(src_dobj);

        assertEquals( "PatientName", src_dobj.getString(s));

        assertEquals( "", result_dobj.getString(s0_1));
        assertEquals( "1", result_dobj.getString(s0_2));
        assertEquals( "1.23.4.38455575335519768996751386654154983966", result_dobj.getString(s0_3));
        assertEquals( "Tum1_LFront", result_dobj.getString(s0_4));
        assertEquals( "MANUAL", result_dobj.getString(s0_5));

        assertEquals( "", result_dobj.getString(s1_1));
        assertEquals( "2", result_dobj.getString(s1_2));
        assertEquals( "1.23.4.38455575335519768996751386654154983966", result_dobj.getString(s1_3));
        assertEquals( "Tum2_Vermis", result_dobj.getString(s1_4));
        assertEquals( "MANUAL", result_dobj.getString(s1_5));

        assertEquals( "", result_dobj.getString(s2_1));
        assertEquals( "3", result_dobj.getString(s2_2));
        assertEquals( "1.23.4.38455575335519768996751386654154983966", result_dobj.getString(s2_3));
        assertEquals( "Tum3_RFront", result_dobj.getString(s2_4));
        assertEquals( "MANUAL", result_dobj.getString(s2_5));

    }


    private ByteArrayInputStream bytes(final String s) {
        return new ByteArrayInputStream(s.getBytes());
    }

    private DicomObjectI createSeqTestObject() {
        final DicomObjectI src_dobj = DicomObjectFactory.newInstance();

        src_dobj.putString(s, "PatientName");

        src_dobj.putString(s0_1, null);
        src_dobj.putString(s0_2, "1");
        src_dobj.putString(s0_3, "1.3.12.2.1107.5.2.6.22750.20111117084936000.0.0.0");
        src_dobj.putString(s0_4, "Tum1_LFront");
        src_dobj.putString(s0_5, "MANUAL");

        src_dobj.putString(s1_1, null);
        src_dobj.putString(s1_2, "2");
        src_dobj.putString(s1_3, "1.3.12.2.1107.5.2.6.22750.20111117084936000.0.0.0");
        src_dobj.putString(s1_4, "Tum2_Vermis");
        src_dobj.putString(s1_5, "MANUAL");

        src_dobj.putString(s2_1, null);
        src_dobj.putString(s2_2, "3");
        src_dobj.putString(s2_3, "1.3.12.2.1107.5.2.6.22750.20111117084936000.0.0.0");
        src_dobj.putString(s2_4, "Tum3_RFront");
        src_dobj.putString(s2_5, "MANUAL");

        return src_dobj;

    }

    private static int[] s = {0x00100010};

    private static int[] s0_1 = {0x30060020, 0, 0x00201040};  // StructureSetROISequence / PositionReferenceIndicator
    private static int[] s0_2 = {0x30060020, 0, 0x30060022};  // StructureSetROISequence / ROINumber
    private static int[] s0_3 = {0x30060020, 0, 0x30060024};  // StructureSetROISequence / ReferencedFrameOfReferenceUID
    private static int[] s0_4 = {0x30060020, 0, 0x30060026};  // StructureSetROISequence / ROIName
    private static int[] s0_5 = {0x30060020, 0, 0x30060036};  // StructureSetROISequence / ROIGenerationAlgorithm

    private static int[] s1_1 = {0x30060020, 1, 0x00201040};  // StructureSetROISequence / PositionReferenceIndicator
    private static int[] s1_2 = {0x30060020, 1, 0x30060022};  // StructureSetROISequence / ROINumber
    private static int[] s1_3 = {0x30060020, 1, 0x30060024};  // StructureSetROISequence / ReferencedFrameOfReferenceUID
    private static int[] s1_4 = {0x30060020, 1, 0x30060026};  // StructureSetROISequence / ROIName
    private static int[] s1_5 = {0x30060020, 1, 0x30060036};  // StructureSetROISequence / ROIGenerationAlgorithm

    private static int[] s2_1 = {0x30060020, 2, 0x00201040};  // StructureSetROISequence / PositionReferenceIndicator
    private static int[] s2_2 = {0x30060020, 2, 0x30060022};  // StructureSetROISequence / ROINumber
    private static int[] s2_3 = {0x30060020, 2, 0x30060024};  // StructureSetROISequence / ReferencedFrameOfReferenceUID
    private static int[] s2_4 = {0x30060020, 2, 0x30060026};  // StructureSetROISequence / ROIName
    private static int[] s2_5 = {0x30060020, 2, 0x30060036};  // StructureSetROISequence / ROIGenerationAlgorithm
}
