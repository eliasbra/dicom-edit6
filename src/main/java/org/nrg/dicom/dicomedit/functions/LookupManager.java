/*
 * DicomEdit: FunctionManager
 * XNAT http://www.xnat.org
 * Copyright (c) 2017, Washington University School of Medicine
 * All Rights Reserved
 *
 * Released under the Simplified BSD.
 */

package org.nrg.dicom.dicomedit.functions;

import org.nrg.dicom.mizer.exceptions.ScriptEvaluationException;
import org.nrg.dicom.mizer.objects.DicomObjectI;
import org.nrg.dicom.mizer.values.AbstractMizerValue;
import org.nrg.dicom.mizer.values.Value;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.core.io.Resource;
import org.springframework.core.io.support.PathMatchingResourcePatternResolver;

import java.io.*;
import java.text.MessageFormat;
import java.util.HashMap;
import java.util.List;
import java.util.Map;
import java.util.Properties;

/**
 * Provide API to all Lookup functions.
 *
 * Singleton class that manages lookup table functions.  *
 */
public class LookupManager {
    private static final Logger logger = LoggerFactory.getLogger(LookupManager.class);
    private static LookupManager lookupManager;
    private static Map<String,Map<String, String>> lookupMaps;

    /**
     * Construct the LookupManager.
     *
     * This is private constructor. Use <code>FunctionManager.getInstance()</code> to get the singleton instance.
     */
    private LookupManager() {
        lookupMaps = new HashMap<>();
        lookupManager = this;
    }

    public void add( String keyType, String key, String value) {
        if( ! lookupMaps.containsKey( keyType)) {
            lookupMaps.put( keyType, new HashMap<String, String>());
        }
        lookupMaps.get(keyType).put( key, value);
    }

    /**
     * Get the value or null.
     *
     * @param keyType
     * @param key
     * @return
     */
    public String get( String keyType, String key) {
        String value = null;
        if( lookupMaps.containsKey( keyType)) {
            value = lookupMaps.get(keyType).get(key);
            if( value == null) {
                logger.debug("Unknown keyType/key: " + keyType + "/" + key);
            }
        }
        else {
            logger.debug("Unknown keyType: " + keyType);
        }
        return value;
    }

    /**
     * Return the singleton instance of the LookupManager.
     *
     * @return LookupManager
     */
    public static LookupManager getInstance() {
        if( lookupManager == null) {
            lookupManager = new LookupManager();
        }
        return lookupManager;
    }

    public void load( String line) {
        if( ! line.isEmpty() && ! line.trim().startsWith("//")) {
            String[] tokens = line.split("=");
            if( tokens.length == 2) {
                String keys = tokens[0].trim();
                String value = tokens[1].trim();
                String[] keyTokens = keys.split("/");
                if( keyTokens.length == 2) {
                    String keyType = keyTokens[0].trim();
                    String key = keyTokens[1];
                    add( keyType, key, value);
                }
                else {
                    logger.debug("Unrecognized key pair '" + keys + "' in line " + line);
                }
            }
            else {
                logger.debug("Unrecognized line: " + line);
            }
        }
    }

    public void load( File file) throws IOException {
        try (BufferedReader reader = new BufferedReader( new FileReader( file))) {
            String line;
            while( (line = reader.readLine()) != null) {
                load( line);
            }
        }
    }

    public void load( String[] lines) throws IOException {
        for( String line: lines) {
            load( line);
        }
    }
}
