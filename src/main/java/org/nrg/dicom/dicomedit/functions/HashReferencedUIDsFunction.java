/*
 * DicomEdit: org.nrg.dicom.dicomedit.functions.UppercaseFunction
 * XNAT http://www.xnat.org
 * Copyright (c) 2017, Washington University School of Medicine
 * All Rights Reserved
 *
 * Released under the Simplified BSD.
 */

package org.nrg.dicom.dicomedit.functions;

import org.nrg.dicom.dicomedit.TagPathFactory;
import org.nrg.dicom.mizer.exceptions.MizerException;
import org.nrg.dicom.mizer.exceptions.ScriptEvaluationException;
import org.nrg.dicom.mizer.exceptions.ScriptEvaluationRuntimeException;
import org.nrg.dicom.mizer.objects.DicomElementI;
import org.nrg.dicom.mizer.objects.DicomObjectI;
import org.nrg.dicom.mizer.objects.DicomObjectVisitor;
import org.nrg.dicom.mizer.tags.TagPath;
import org.nrg.dicom.mizer.values.AbstractMizerValue;
import org.nrg.dicom.mizer.values.UIDValue;
import org.nrg.dicom.mizer.values.Value;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import java.text.MessageFormat;
import java.util.List;

/**
 * Hash the referenced uids and prepend the prefix.
 *
 */
public class HashReferencedUIDsFunction extends AbstractScriptFunction {

    private static final Logger logger = LoggerFactory.getLogger(HashReferencedUIDsFunction.class);

    public HashReferencedUIDsFunction() {
        super("hashReferencedUIDs", AbstractScriptFunction.DEFAULT_NAMESPACE, "Usage: ", "Description: ");
    }

    @Override
    public Value apply(final List<Value> values, DicomObjectI dicomObject) throws ScriptEvaluationException {

        if (values.size() > 1) {

            final String prefix = values.get(0).asString();
            final List<Value> tagPathValues = values.subList(1, values.size());

            DicomObjectVisitor visitor = new DicomObjectVisitor() {
                @Override
                public void visitTag(TagPath tagPath, DicomElementI dicomElement, DicomObjectI dicomObject) {
                    logger.debug(tagPath + " : " + dicomElement);
                    for (Value v : tagPathValues) {
                        try {
                            TagPath tp = TagPathFactory.createInstance(v.asString());
                            if (tp.isMatch(tagPath)) {
                                String uid = dicomObject.getString( dicomElement.tag());
                                String newUID = getHashedUID( prefix, uid);
                                dicomObject.putString( dicomElement.tag(), newUID);
                                return;
                            }
                        } catch (MizerException e) {
                            String msg = "function arg tagPath: " + v.asString();
                            logger.error(msg);
//                            throw new ScriptEvaluationException( msg, e);
                        }
                    }
                }

                @Override
                public void visitSequenceTag(TagPath tagPath, DicomElementI dicomElement, DicomObjectI dicomObject) { /* ignore */ }
            };

            visitor.visit(dicomObject);

        } else {
            tooFewArguments( values);
        }

        return AbstractMizerValue.VOID;
    }

    /**
     * Generate a new UID by appending the hash of the provided value to the prefix.
     *
     * @param prefix The prefix of the new UID.  (Handles trailing "." appropriately)
     * @param value The value to be hashed.
     * @return the new UID.
     * @throws ScriptEvaluationException
     */
    protected String getHashedUID( String prefix, String value) throws ScriptEvaluationException {
        String hash = UIDValue.getHashedUIDValue( value).asString();
        hash = (hash.startsWith("2.25."))? hash.substring( "2.25.".length()): hash;
        String newUID;
        if( prefix.endsWith(".")) {
            newUID = prefix + hash;
        }
        else {
            newUID = prefix + "." + hash;
        }
        return newUID;
    }

    protected void tooFewArguments(List<Value> values) {

        String arguments;
        switch (values.size()) {
            case 0:
                arguments = "null";
                break;
            case 1:
                arguments = values.get(0).asString();
                break;
            default:
                arguments = null;
        }

        MessageFormat message = new MessageFormat("hashReferencedUIDs illegal arguments: '{0}'. Expect uid-prefix-string, tagpath-string");
        throw new ScriptEvaluationRuntimeException(message.format(arguments));
    }

}
