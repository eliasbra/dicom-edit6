/*
 * DicomEdit: org.nrg.dicom.dicomedit.ScriptRunner
 * XNAT http://www.xnat.org
 * Copyright (c) 2016, Washington University School of Medicine
 * All Rights Reserved
 *
 * Released under the Simplified BSD.
 */

package org.nrg.dicom.dicomedit;

import java.io.File;
import java.io.FileInputStream;
import java.io.FileOutputStream;
import java.io.InputStream;

/**
 * An app to quickly apply specified scripts to specified data.
 *
 * First argument is path to the anon script.
 * Second and later arguments are paths to DICOM objects to which the script will be applied.
 *
 * Modified DICOM objects will be written next to the input object with a modified file name.
 *
 */
public class Main {

    public static void main(final String args[]) {
        final ScriptApplicator applicator;

        try {
            try (InputStream is = (0 == args.length || "-".equals(args[0]) ? System.in : new FileInputStream(args[0]))) {
//            try (InputStream is = new ByteArrayInputStream("(0080,1110)/(0010,0010)/(0019,[SIEMENS]10) := (0010,0020)\n".getBytes())) {
//            try (InputStream is = new ByteArrayInputStream("(0080,1110)[0]/(0010,0010)[0]/(0019,[SIEMENS]10) := (0010,0020)\n".getBytes())) {
//            try (InputStream is = new ByteArrayInputStream("(0080,1110)[0]/(0010,0010)[0]/(0019,[SIEMENS]10) := (0054,0410)[0]/(0054,0412)[0]/(0008,0105)\n".getBytes())) {
//            try (InputStream is = new ByteArrayInputStream("(0008,0020) ~ \"20\\\\d\\\\d\\\\d\\\\d\\\\d\\\\d\" : (0010,4000) := \"Session 20XX\"\n".getBytes())) {
                applicator = new ScriptApplicator(is);
            }

            for (int i = 1; i < args.length; i++) {
                final File f = new File(args[i]);
                try (FileOutputStream out = new FileOutputStream(mapFileName(f))) {
                    applicator.apply( f).write(out);
                }
            }
        } catch (Exception e) {
            e.printStackTrace();
        }
    }

    private static File mapFileName(File f) {
        final String presuffix = "-mod";
        final File dir = f.getParentFile();
        final StringBuilder name = new StringBuilder(f.getName());
        final int presuffixLoc = name.lastIndexOf(".");
        if (presuffixLoc < 0) {
            name.append(presuffix);
        } else {
            name.insert(presuffixLoc, presuffix);
        }
        return new File(dir, name.toString());
    }

}
