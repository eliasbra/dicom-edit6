package org.nrg.dicom.dicomedit;

import org.antlr.v4.runtime.*;
import org.antlr.v4.runtime.tree.ParseTree;
import org.nrg.dicom.mizer.exceptions.MizerException;
import org.nrg.dicom.mizer.scripts.AbstractMizerScript;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import java.io.*;
import java.text.MessageFormat;
import java.util.List;

/**
 * Model of DicomEdit version 6 script.
 *
 * Currently stored as a list of String statements.
 *
 * Uses the DE6Lexer and DE6Parser to create ParseTree of a script and used repeatedly by
 * a {@link ScriptApplicator}
 */
public class DE6Script extends AbstractMizerScript {
    public DE6Script(final String script) throws MizerException {
        this(new StringReader(script));
    }

    public DE6Script(final InputStream input) throws MizerException {
        this(new InputStreamReader(input));
    }

    public DE6Script(final Reader reader) throws MizerException {
        super(reader);
        _tree = createParseTree();
    }

    public DE6Script(final List<String> statements) throws MizerException {
        super(statements);
        _tree = createParseTree();
    }

    protected ParseTree getParseTree() {
        return _tree;
    }

    @Override
    protected void initialize() {
        final DiscoverVariablesTreeVisitor variablesTreeVisitor = new DiscoverVariablesTreeVisitor();
        variablesTreeVisitor.visit(_tree);
        final DiscoverReferencedTagsTreeVisitor tagsTreeVisitor = new DiscoverReferencedTagsTreeVisitor();
        tagsTreeVisitor.visit(_tree);
        setEntities(variablesTreeVisitor.getVariables(), variablesTreeVisitor.getExternalVariables(), tagsTreeVisitor.getReferenedTags());
    }

    protected ParseTree createParseTree() throws MizerException {
        logger.debug("Parsing DE6 script syntax.");
        try (final InputStream input = getInputStream()) {
            return new DE6Parser(new CommonTokenStream(new DE6Lexer(new ANTLRInputStream(input)))) {{
                addErrorListener(new BaseErrorListener() {
                    @Override
                    public void syntaxError(final Recognizer<?, ?> recognizer,
                                            final Object offendingSymbol,
                                            final int line,
                                            final int position,
                                            final String message,
                                            final RecognitionException e) {
                        final String errorMessage = MessageFormat.format("Failed to parse at {0}:{1} due to {2}", line, position, message);
                        logger.error(errorMessage, e);
                        throw new IllegalStateException(errorMessage, e);
                    }
                });
            }}.script();
        } catch (IOException e) {
            throw new MizerException("Failed creating DE6Lexer.", e);
        }
    }

    private static final Logger logger = LoggerFactory.getLogger(DE6Script.class);

    private final ParseTree _tree;
}