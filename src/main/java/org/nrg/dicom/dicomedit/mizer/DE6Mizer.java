package org.nrg.dicom.dicomedit.mizer;

import org.nrg.dicom.dicomedit.DE6Script;
import org.nrg.dicom.dicomedit.ScriptApplicator;
import org.nrg.dicom.mizer.exceptions.MizerContextException;
import org.nrg.dicom.mizer.exceptions.MizerException;
import org.nrg.dicom.mizer.objects.DicomObjectI;
import org.nrg.dicom.mizer.service.MizerContext;
import org.nrg.dicom.mizer.service.VersionString;
import org.nrg.dicom.mizer.service.impl.AbstractMizer;
import org.nrg.dicom.mizer.service.impl.MizerContextWithScript;
import org.nrg.dicom.mizer.variables.Variable;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.stereotype.Component;

import java.io.IOException;
import java.io.InputStream;
import java.text.MessageFormat;
import java.util.Arrays;
import java.util.HashSet;
import java.util.List;
import java.util.Set;

/**
 * Handle anonymization with DicomEdit v6 scripts.
 */
@Component
public class DE6Mizer extends AbstractMizer {

    private static final List<VersionString> supportedVersions = Arrays.asList(new VersionString("6.0"), new VersionString("6.1"), new VersionString("6.2"));
    private static final Logger              logger            = LoggerFactory.getLogger(DE6Script.class);

    public DE6Mizer() {
        super(supportedVersions);
    }

    /**
     * {@inheritDoc}
     */
    @Override
    public void aggregate(final MizerContext context, final Set<Variable> variables) {
        for (final Variable variable : variables) {
            context.setElement(variable.getName(), variable);
        }
    }

    /**
     * {@inheritDoc}
     */
    @Override
    public Set<Integer> getScriptTags(final List<MizerContext> contexts) {
        return new HashSet<Integer>() {{
            for (final MizerContext context : contexts) {
                addAll(getScriptTags(context));
            }
        }};
    }

    /**
     * {@inheritDoc}
     */
    @Override
    public Set<Integer> getScriptTags(final MizerContext context) {
        final Set<Integer> tags = new HashSet<>();
        if (context instanceof MizerContextWithScript) {
            final MizerContextWithScript scriptContext = (MizerContextWithScript) context;
            try {
                final DE6Script script = new DE6Script(scriptContext.getScript());
                tags.addAll(script.getReferencedTags());
            } catch (MizerException e) {
                logger.warn("Error looking for referenced tags in script:\n" + scriptContext.getScriptAsString(), e);
            }
        }
        return tags;
    }

    /**
     * {@inheritDoc}
     */
    @Override
    public void getReferencedVariables(final MizerContext context, final Set<Variable> variables) throws MizerContextException {
        if (context instanceof MizerContextWithScript) {
            final MizerContextWithScript scriptContext = (MizerContextWithScript) context;
            try {
                final DE6Script script = new DE6Script(scriptContext.getScript());
                variables.addAll(script.getVariables().values());
            } catch (MizerException e) {
                logger.warn("Error looking for referenced variables in script:\n" + scriptContext.getScriptAsString(), e);
                if (e instanceof MizerContextException) {
                    throw (MizerContextException) e;
                }
                throw new MizerContextException(scriptContext, e);
            }
        }
    }

    @Override
    protected void anonymizeImpl(final DicomObjectI dicomObject, final MizerContextWithScript context) throws MizerException {
        final List<String> statements = context.getScript();
        try {
            final DE6Script script = new DE6Script(statements);
            try (InputStream input = script.getInputStream()) {
                final ScriptApplicator applicator = new ScriptApplicator(input);

                for (final String name : applicator.getExternalVariableNames()) {
                    final String value = (String) context.getElement(name);
                    if (value == null) {
                        throw new MizerException(MessageFormat.format("Missing value for script-required variable: {0}", name));
                    } else {
                        applicator.setVariable(name, value);
                    }
                }

                applicator.apply(dicomObject);
            }
        } catch (IOException e) {
            throw new MizerException(e);
        }
    }

    @Override
    protected String getMeaning() {
        return "XNAT DicomEdit 6 Script";
    }

    @Override
    protected String getSchemeDesignator() {
        return "XNAT";
    }

    @Override
    protected String getSchemeVersion() {
        return "1.0";
    }

    @Override
    public void setContext(MizerContextWithScript script) throws MizerException {

    }

    @Override
    public void removeContext(MizerContextWithScript script) {

    }
}
