package org.nrg.dicom.dicomedit;

import org.antlr.v4.runtime.*;
import org.antlr.v4.runtime.tree.ParseTree;
import org.nrg.dicom.mizer.exceptions.MizerException;
import org.nrg.dicom.mizer.scripts.AbstractMizerScript;
import org.nrg.dicom.mizer.tags.Tag;
import org.nrg.dicom.mizer.tags.TagPath;
import org.nrg.dicom.mizer.tags.TagSequence;
import org.nrg.dicom.mizer.values.ConstantValue;
import org.nrg.dicom.mizer.values.Value;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import java.io.*;
import java.text.MessageFormat;
import java.util.List;

/**
 * Create TagPath instances utilizing the DE6Lexer and Parser.
 *
 * DE6 Parser already knows how to parse TagPaths from strings so use them.
 *
 */
public class TagPathFactory  {

    public static TagPath createInstance(final String string) throws MizerException {

        ParseTree parseTree = createParseTree( string);
        DicomEditParseTreeVisitor visitor = new DicomEditParseTreeVisitor();
        Value v = visitor.visit( parseTree);
        return (TagPath) v.asObject();
    }

    protected static ParseTree createParseTree( String string) throws MizerException {
        logger.debug("Parsing DE6 tagpath syntax.");
        try (final InputStream input = new ByteArrayInputStream( string.getBytes())) {
            return new DE6Parser(new CommonTokenStream(new DE6Lexer(new ANTLRInputStream(input)))) {{
                addErrorListener(new BaseErrorListener() {
                    @Override
                    public void syntaxError(final Recognizer<?, ?> recognizer,
                                            final Object offendingSymbol,
                                            final int line,
                                            final int position,
                                            final String message,
                                            final RecognitionException e) {
                        final String errorMessage = MessageFormat.format("Failed to parse tagpath at {0}:{1} due to {2}", line, position, message);
                        logger.error(errorMessage, e);
                        throw new IllegalStateException(errorMessage, e);
                    }
                });
            }}.tagpath();
        } catch (IOException e) {
            throw new MizerException("Failed creating DE6 Tagpath parser.", e);
        }
    }

    private static final Logger logger = LoggerFactory.getLogger(TagPathFactory.class);

}