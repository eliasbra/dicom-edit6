/*
 * DicomEdit: DiscoverVariablesTreeVisitor
 * XNAT http://www.xnat.org
 * Copyright (c) 2017, Washington University School of Medicine
 * All Rights Reserved
 *
 * Released under the Simplified BSD.
 */

package org.nrg.dicom.dicomedit;

import com.google.common.collect.ImmutableMap;
import org.nrg.dicom.mizer.variables.BasicVariable;
import org.nrg.dicom.mizer.variables.Variable;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import java.util.HashMap;
import java.util.Map;

/**
 * Walk the parse tree and discover all referenced variables.
 *
 * Establishes a list of all defined variables.
 * Establishes a list of all variables that are referred to in the script but whose value is not set by the script.
 * These
 * values must be passed in from an external source.
 */
public class DiscoverVariablesTreeVisitor extends DE6ParserBaseVisitor<Void> {
    public Map<String, Variable> getVariables() {
        return ImmutableMap.copyOf(_variables);
    }

    public Map<String, Variable> getExternalVariables() {
        return ImmutableMap.copyOf(_externalVariables);
    }

    @Override
    public Void visitInitialization(final DE6Parser.InitializationContext context) {
        logger.debug("Encountered initialization: " + context.getText());

        final String id = context.ID().getText();
        visit(context.getChild(2));

        logger.debug(String.format("Set variable: name= %s", id));
        _variables.put(id, new BasicVariable(id));
        return null;
    }

    @Override
    public Void visitIdTerm(final DE6Parser.IdTermContext context) {
        logger.debug("Encountered idTerm.");
        final String id = context.variable().getText();
        if (!_variables.containsKey(id)) {
            final Variable variable = new BasicVariable(id);
            _externalVariables.put(id, variable);
            _variables.put(id, variable);
        }
        return null;
    }

    private static final Logger logger = LoggerFactory.getLogger(DiscoverVariablesTreeVisitor.class);

    private Map<String, Variable> _variables         = new HashMap<>();
    private Map<String, Variable> _externalVariables = new HashMap<>();
}
