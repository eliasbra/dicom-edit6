/*
 * DicomEdit: DicomEditParseTreeVisitor
 * XNAT http://www.xnat.org
 * Copyright (c) 2017, Washington University School of Medicine
 * All Rights Reserved
 *
 * Released under the Simplified BSD.
 */

package org.nrg.dicom.dicomedit;

import org.antlr.v4.runtime.ParserRuleContext;
import org.antlr.v4.runtime.misc.ParseCancellationException;
import org.nrg.dicom.mizer.exceptions.ScriptEvaluationException;
import org.nrg.dicom.mizer.exceptions.ScriptEvaluationRuntimeException;
import org.nrg.dicom.mizer.objects.DicomObjectI;
import org.nrg.dicom.dicomedit.functions.FunctionManager;
import org.nrg.dicom.mizer.tags.*;
import org.nrg.dicom.mizer.values.ConstantValue;
import org.nrg.dicom.mizer.values.IntegerValue;
import org.nrg.dicom.mizer.values.Value;
import org.nrg.dicom.mizer.variables.BasicVariable;
import org.nrg.dicom.mizer.variables.Variable;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import java.text.MessageFormat;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

import static org.nrg.dicom.mizer.values.AbstractMizerValue.VOID;

/**
 * Implementation of a DE6 grammar's parse-tree visitor, where in we add the semantics to the syntax.
 *
 */
public class DicomEditParseTreeVisitor extends DE6ParserBaseVisitor<Value> {

    private Map<String, Variable> variables = new HashMap<String, Variable>();
    private DicomObjectI dicomObject;

    private static final Logger logger = LoggerFactory.getLogger(DicomEditParseTreeVisitor.class);

    public void setDicomObject( DicomObjectI dicomObject) {
        this.dicomObject = dicomObject;
    }

    public Map<String, Variable> getVariables() {
        return variables;
    }

    public void setVariable(final String name, final Value value) {
        final Variable variable;
        if (getVariables().containsKey(name)) {
            variable = getVariable(name);
        } else {
            variable = new BasicVariable(name);
        }
        variable.setValue(value);
        variables.put( name, variable);
    }

    public Variable getVariable(String name) {
        return variables.get( name);
    }

    @Override
    public Value visitScript(DE6Parser.ScriptContext ctx) {
        logger.debug("Encountered script.");
        for( int i = 0; i < ctx.statement().size(); i++) {
            logger.debug("visit statement " + i + ": " + ctx.statement(i).getText());
            this.visit(ctx.statement(i));
        }
        return new ConstantValue(null);
    }

    @Override
    public Value visitStatement(DE6Parser.StatementContext ctx) {
        this.visit(ctx.getChild(0));
        return new ConstantValue(null);
    }

    @Override
    public Value visitVersion(DE6Parser.VersionContext ctx) {
        String versionString = ctx.STRING().toString();
        // remove the enclosing quotes on the string
        versionString = versionString.replaceAll("\"", "");
        VersionManager vm = VersionManager.getInstance();
        if( ! vm.isKnownVersion( versionString)) {
            String msg = MessageFormat.format("Unsupported version: {0}. Supported versions are {1}", versionString, vm.getSupportedVersionStrings());
            logger.error(msg);
            throw new ScriptEvaluationRuntimeException(msg);
        }
        return new ConstantValue(versionString);
    }

    @Override
    public Value visitInitialization(DE6Parser.InitializationContext ctx) {
        logger.debug("Encountered initialization: " + ctx.getText());
        String id = ctx.ID().getText();
        Value value = visit(ctx.getChild(2));
        logger.debug(String.format("Set variable: name= %s, value= %s", id, value));
        variables.put(id, new BasicVariable(id, value));
        return value;
    }

    @Override
    public Value visitDescribeNamedVariable(DE6Parser.DescribeNamedVariableContext ctx) {
        String name = ctx.ID().getText();
        Variable variable = getVariable( name);
        if( variable == null) {
            variable = new BasicVariable( name);
            variables.put( name, variable);
        }
        String description = extractString(ctx.STRING().toString());
        variable.setDescription( description);
        return new ConstantValue(description);
    }

    @Override
    public Value visitDescibeHiddenVariable(DE6Parser.DescibeHiddenVariableContext ctx) {
        String name = ctx.ID().getText();
        Variable variable = getVariable( name);
        if( variable == null) {
            variable = new BasicVariable( name);
            variables.put( name, variable);
        }
        final String hiddenToken = extractString(ctx.HIDDEN_TOKEN().toString());
        variable.setIsHidden( true);
        return new ConstantValue(hiddenToken);
    }

    @Override
    public Value visitExport(DE6Parser.ExportContext ctx) {
        String name = ctx.ID().getText();
        Variable variable = getVariable( name);
        if( variable == null) {
            variable = new BasicVariable( name);
            variables.put( name, variable);
        }
        String exportString = extractString(ctx.STRING().toString());
        variable.setExportField( exportString);
        return new ConstantValue(exportString);
    }

    @Override
    public Value visitAssignment(DE6Parser.AssignmentContext ctx) {
        logger.debug("Encountered assignment: " + ctx.getText());
        Value assignedValue = visit( ctx.value());
        if( VOID.equals(assignedValue)) {
            throw new IllegalArgumentException("Assigned value is VOID.");
        }
        String assignedString = assignedValue.asString();

        Value lvalue = visit(ctx.lvalue());
        if( VOID.equals(lvalue)) {
            throw new IllegalArgumentException("Value assigned to can not be VOID.");
        }
        TagPath tp = (TagPath) lvalue.asObject();

        logger.debug("Assigned: " + tp + " = " + assignedString);
        dicomObject.assign( tp, assignedString);

        return assignedValue;
    }

    @Override
    public Value visitTag(DE6Parser.TagContext ctx) {
        return visit( ctx.getChild(0));
    }

    @Override
    public Value visitTagpath(DE6Parser.TagpathContext ctx) {
        TagPath tagPath = new TagPath();
        List<DE6Parser.Seq_elementContext> seq_elementContexts = ctx.seq_element();
        for(DE6Parser.Seq_elementContext sctx: seq_elementContexts) {
            Value v = visit(sctx);
            tagPath.addTag( (Tag) v.asObject());
        }
        if( ctx.element() != null) {
            Value v = visit( ctx.element());
            tagPath.addTag( (Tag) v.asObject());
        }
        else {
            logger.debug("TagPath must include an element");
        }
        return new ConstantValue(tagPath);
    }

    @Override
    public Value visitSeq_element(DE6Parser.Seq_elementContext ctx) {
        Tag tag;
        if( ctx.element() != null) {
            Tag t = (Tag) visit(ctx.element()).asObject();
            String itemNumberString = null;
            if (ctx.itemnumber() != null) {
                itemNumberString = visit(ctx.itemnumber()).toString();
            }
            tag = new TagSequence(t, itemNumberString);
        }
        else {
            tag = (Tag) visit(ctx.seq_wildcard()).asObject();
        }
        return new ConstantValue(tag);
    }

    @Override
    public Value visitSeq_wildcard(DE6Parser.Seq_wildcardContext ctx) {
        String s = ctx.getText();
        return new ConstantValue(new TagSequenceWildcard(s));
    }

    @Override
    public Value visitItemnumber(DE6Parser.ItemnumberContext ctx) {
        Value value;
        if( ctx.INTEGER() != null) {
            value = new IntegerValue(ctx.INTEGER().getText());
        }
        else if( ctx.ITEM_WILDCARD() != null) {
            value = new ConstantValue(ctx.ITEM_WILDCARD().getText());
        }
        else {
            value = new ConstantValue();
        }
        return value;
    }

    private boolean isLValueContext(ParserRuleContext ctx) {
        boolean b = false;
        do {
            if( ctx instanceof DE6Parser.LvalueContext) {
                b = true;
                break;
            }
        } while (( ctx = ctx.getParent()) != null);
        return b;
    }

    @Override
    public Value visitPublic_tag(DE6Parser.Public_tagContext ctx) {
        String group = ctx.PUBLIC_GROUP().getText();
        group = group.substring(1);  // strip leading '('
        String element = ctx.PUBLIC_ELEMENT().getText();
        element = element.substring(0, element.length()-1);  // strip trailing ')'

        TagPublic tag = new TagPublic(group, element);
        return new ConstantValue(tag);
    }

    /**
     * This hard codes some of the syntax which ain't a good thing.
     * TODO: figure out how to not need the grouping characters in the parser or how to grab those characters by
     * reference.
     *
     * @param ctx The context containing text.
     * @return The value for the string terminator.
     */
    @Override
    public Value visitPvt_tag(DE6Parser.Pvt_tagContext ctx) {
        String group = ctx.PVT_GROUP().getText();
        group = group.substring(1);  // strip leading '('
        String element = ctx.PVT_ELEMENT().getText();
        element = element.substring(0, element.length()-1);  // strip trailing ')'
        String pvtCreatorID = element.substring(1, element.indexOf("}"));
        String subindex = element.substring(element.indexOf("}")+1);

        TagPrivate tag = new TagPrivate(group, pvtCreatorID, subindex);
        return new ConstantValue(tag);
    }

    /**
     * Map the private tag context into a private tag in block 00. This "generic"
     * private tag will then need to be resolved into its actual block in the dicom object.
     *
     * @param ctx
     * @return
     */
    private int getGenericPrivateTag( DE6Parser.Pvt_tagContext ctx) {
        String group = ctx.PVT_GROUP().getText();
        group = group.substring(1);  // strip leading '('
        String element = ctx.PVT_ELEMENT().getText();
        element = element.substring(0, element.length()-1);  // strip trailing ')'

        String subindex = element.substring(element.indexOf("]")+1);

        return Integer.parseInt(group + "00" + subindex, 16);
    }

    private String getPrivateTagCreatorID( DE6Parser.Pvt_tagContext ctx) {
        String element = ctx.PVT_ELEMENT().getText();

        String pvtCreator = element.substring(1, element.indexOf("]"));

        return pvtCreator;
    }

    @Override
    public Value visitMethod( DE6Parser.MethodContext ctx) {
        Value value;
        String functionName = null;
        try {
            logger.debug("Encountered method term: " + ctx.getText());
            functionName = ctx.ID().getText();
            DE6Parser.TermlistContext c = ctx.termlist();
            Value termListValue = (c == null) ? new ConstantValue(new ArrayList<Value>()) : visit(ctx.termlist());
            List<Value> args = termListValue.asValueList();
            value = evaluateFunction(functionName, args, dicomObject);
            logger.debug("Function " + functionName + " = " + value.asString());
            return value;
        } catch( ScriptEvaluationException e) {
            String msg = "Error evaluating " + ("null".equals(functionName)? "unknown": functionName);
            logger.debug( msg);
            logger.debug( e.getMessage());
            throw new ScriptEvaluationRuntimeException(msg, e);
        }
    }

    @Override
    public Value visitFunctionTerm(DE6Parser.FunctionTermContext ctx) {
        Value value;
        String functionName = null;
        try {
            logger.debug("Encountered function term: " + ctx.getText());
            functionName = ctx.function().ID().getText();
            DE6Parser.TermlistContext c = ctx.function().termlist();
            Value termListValue = (c == null) ? new ConstantValue(new ArrayList<Value>()) : visit(ctx.function().termlist());
            List<Value> args = termListValue.asValueList();
            value = evaluateFunction(functionName, args, dicomObject);
            logger.debug("Function " + functionName + " = " + value.asString());
            return value;
        } catch( ScriptEvaluationException e) {
            String msg = "Error evaluating " + ("null".equals(functionName)? "unknown": functionName);
            logger.debug( msg);
            logger.debug( e.getMessage());
            throw new ScriptEvaluationRuntimeException(msg, e);
        }
    }

    @Override
    public Value visitTermlist(DE6Parser.TermlistContext ctx) {
        logger.debug("Encountered term list.");
        List<Value> values = new ArrayList<Value>();
        int termCount = ctx.term().size();
        for (int i = 0; i < termCount; i++) {
            values.add(visit(ctx.term().get(i)));
        }
        return new ConstantValue(values);
    }

    @Override
    public Value visitValue(DE6Parser.ValueContext ctx) {
        return this.visit(ctx.getChild(0));
    }

    /**
     * Return the value of the string.
     *
     * Having problems when the string is a regex?  Look here first!
     *
     * @param ctx The context containing text.
     * @return The value for the string terminator.
     */
    @Override
    public Value visitStringTerm(DE6Parser.StringTermContext ctx) {
        String str = ctx.getText();
        // strip quotes
        str = str.substring(1, str.length() - 1).replace("\"\"", "\"");
        // Ugh, java strings with backslashes are confusing.
        // User has to write the string with backslashes (likely in regexs) escaped but antlr seems to provide the
        // string with both the escaping and the escaped \.  Remove the escaping \
        str = str.replace( "\\\\", "\\");
        return new ConstantValue(str);
    }

    @Override
    public Value visitIdTerm(DE6Parser.IdTermContext ctx)  {
        logger.debug("Encountered idTerm.");
//        String id = ctx.ID().getText();
        String id = ctx.variable().getText();
        if( variables.containsKey(id)) {
            return variables.get( id).getValue();
        }
        else {
            throw new ParseCancellationException("Unknown variable: " + id);
        }
    }

    @Override
    public Value visitNumberTerm(DE6Parser.NumberTermContext ctx) {
        return new ConstantValue(ctx.getText());
    }

    @Override
    public Value visitTagPathTerm(DE6Parser.TagPathTermContext ctx) {
        Value vtp = visit(ctx.tagpath());
        TagPath tp = (TagPath) vtp.asObject();
        Value v;
        if( tp.isSingular()) {
            v = new ConstantValue(dicomObject.getString(tp));
        }
        else {
            String msg = "Path matching multiple tags not allowed as term: " + tp.toString();
            logger.warn( msg);
            v = VOID;
//            throw new ScriptEvaluationException( msg);
        }
        return v;
    }

    private Value evaluateFunction(String functionName, List<Value> args, DicomObjectI dicomObject) throws ScriptEvaluationException {
        FunctionManager functionManager = FunctionManager.getInstance();
        Value v = functionManager.execute( functionName, args, dicomObject);
        return v;
    }

    @Override
    public Value visitDeletion(DE6Parser.DeletionContext ctx) {
        logger.debug("Encountered deletion: " + ctx.getText());
        Value lvalue = visit(ctx.lvalue());
        TagPath tp = (TagPath) lvalue.asObject();

        dicomObject.delete( tp);

        return VOID;
    }

    @Override
    public Value visitRemoveAllPrivateTags(DE6Parser.RemoveAllPrivateTagsContext ctx) {
        dicomObject.deleteAllPrivateTags();
        return VOID;
    }

    @Override
    public Value visitConditional_statement(DE6Parser.Conditional_statementContext ctx) {
        Value condition = visit(ctx.constraint());
        Value v = VOID;
        if( condition.asBoolean() == true) {
            v = visit(ctx.action(0));
        }
        else {
            if( ctx.action().size() == 2) {
                v = visit( ctx.action(1));
            }
        }
        return v;
    }

    @Override
    public Value visitConstraint(DE6Parser.ConstraintContext ctx) {
        Value value = visit(ctx.condition());
//        List<DE6Parser.ConditionContext> conditions = ctx.condition();
//        boolean doAction = true;
//        for( DE6Parser.ConditionContext conditionContext: conditions) {
//            Value v = visit(conditionContext);
//            if( v.asBoolean() == false) {
//                doAction = false;
//                break;
//            }
//        }

//        Value value = Value.VOID;
//        if( doAction) {
//            DE6Parser.StatementContext statementContext = (DE6Parser.StatementContext) ctx.getParent();
//            value = visit( statementContext.action());
//        }
        return value;
    }

    /**
     * Return result of condition evaluation.
     * A value of 'null' will not match anything.
     *
     * @param ctx
     * @return boolean result of condition evaluation.
     */
    @Override
    public Value visitCondition(DE6Parser.ConditionContext ctx) {
        String op = ctx.conditionOperator().getText();
        String s1 = visit(ctx.value(0)).asString();
        String s2 = visit(ctx.value(1)).asString();
        final boolean equals;
        switch (op) {
            case "=":
                equals = (s1 != null)? s1.equals(s2): false;
                break;
            case "!=":
                equals = (s1 != null)? ! s1.equals(s2): true;
                break;
            case "~":
                equals = (s1 != null)? s1.matches(s2): false;
                break;
            case "!~":
                equals = (s1 != null)? ! s1.matches(s2): true;
                break;
            default:
                equals = false;
                logger.warn("Unknown conditional operator: " + op);
        }
        return new ConstantValue(equals);
    }

    /**
     * Remove surrounding quotes.
     *
     * TODO: remove internal escaped quotes.
     *
     * @param s
     * @return
     */
    private static String extractString( String s) {
        final StringBuilder sb = new StringBuilder(s);

        sb.deleteCharAt(0); // remove leading and trailing quotes
        sb.deleteCharAt(sb.length() - 1);
        return sb.toString();
    }

}
