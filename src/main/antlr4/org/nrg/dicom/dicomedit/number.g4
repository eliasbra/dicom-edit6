
grammar number;

script	:	separator? EOF
	|	separator? statement (separator statement)* separator? EOF
	;

separator
	:	(COMMENT? NEWLINE)+
	;

statement: (number | id | tagpath) ;

number : INTEGER # intvalue
       | FLOAT   # floatvalue;

id : ID ;

element: public_tag | pvt_tag ;
seq_element: (element itemnumber?) | seq_wildcard ;

tagpath: (element | seq_element) | ((seq_element '/')+ (element | seq_element)) ;

seq_wildcard: SEQ_WILDCARD;

public_tag : PUBLIC_GROUP ',' PUBLIC_ELEMENT ;
pvt_tag    : PVT_GROUP ',' PVT_ELEMENT ;

//itemnumber: '[' ITEMNUMBER ']';
//
//ITEMNUMBER: INTEGER | ITEM_WILDCARD ;
itemnumber: '[' (INTEGER | ITEM_WILDCARD) ']' ;

PUBLIC_GROUP   : '(' EVEN_HEXWORD ;
PVT_GROUP      : '(' ODD_HEXWORD ;
PUBLIC_ELEMENT : HEXWORD ')';
PVT_ELEMENT    : '[' ID ']' HEXDIGIT HEXDIGIT ')';

INTEGER	:	'0' | ('-'? NON_ZERO_DIGIT DIGIT*);
FLOAT:  '-'? ( '0' '.' DIGIT+ | NON_ZERO_DIGIT DIGIT* '.' DIGIT+) ;

ID  :	(LETTER|'_') (LETTER|DIGIT|'_')*
    ;

COMMENT
    :   '//' ~('\n'|'\r')*
    ;

NEWLINE	:	'\r'? '\n';

WS : [ \t]+ -> skip ; // skip spaces, tabs

STRING
    :  '"' ( ESC_SEQ | ~('\\'|'"') )* '"'
    ;

HEXDIGIT_WILDCARD: [Xx] ;

ITEM_WILDCARD : [%] ;
SEQ_WILDCARD : [*?+.] ;

COMMA : ',' ;

ECHO : 'echo' ;
CONSTRAINS :	':';
EQUALS	:	'=';
MATCHES	:	'~';
ASSIGN	:	':=';
DESCRIBE:	'describe';
HIDDEN_TOKEN	:	'hidden';
EXPORT  :	'export';

fragment ODD_HEXDIGIT  :	[13579bBdDfF] | [#];

fragment EVEN_HEXDIGIT :	[02468aAcCeE] | [@];

fragment HEXDIGIT : ODD_HEXDIGIT | EVEN_HEXDIGIT | HEXDIGIT_WILDCARD ;

fragment EVEN_HEXWORD : HEXDIGIT HEXDIGIT HEXDIGIT EVEN_HEXDIGIT ;
fragment ODD_HEXWORD  : HEXDIGIT HEXDIGIT HEXDIGIT ODD_HEXDIGIT ;
fragment HEXWORD      : HEXDIGIT HEXDIGIT HEXDIGIT HEXDIGIT ;

fragment LETTER : ('a'..'z' | 'A'..'Z') ;

fragment DIGIT	: ('0' | NON_ZERO_DIGIT);
fragment NON_ZERO_DIGIT : [1-9] ;

fragment ESC_SEQ
    :   '\\' ('b'|'t'|'n'|'f'|'r'|'\"'|'\''|'\\')
    |   UNICODE_ESC
    |   OCTAL_ESC
    ;

fragment OCTAL_ESC
    :   '\\' ('0'..'3') ('0'..'7') ('0'..'7')
    |   '\\' ('0'..'7') ('0'..'7')
    |   '\\' ('0'..'7')
    ;

fragment UNICODE_ESC
    :   '\\' 'u' HEXDIGIT HEXDIGIT HEXDIGIT HEXDIGIT
    ;
